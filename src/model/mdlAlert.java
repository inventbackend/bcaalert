package model;

public class mdlAlert {
	public String AlertID;
	public String AlertDate;
	public String Flag;
	public String TransactionID;
	public String SerialNumber;
	public String BranchCode;
	public String WSID;
	public String Counter;
	public String TransactionType;
	public String AccountType;
	public String AlertType;
	public String CustomerName;
	public String CustomerAccountNo;
	public String CIN;
	public String ProductRecommendation;
	public String SalesProduct;

}
