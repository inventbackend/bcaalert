package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class AlertAdapter {
    final static Logger logger = LogManager.getLogger(AlertAdapter.class);

    public static String CheckAlertStatus(String AlertID) {
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	String status = "";
	try {
	    // define connection
	    connection = database.RowSetAdapter.getConnectionWL();
	    String sql = "SELECT STATUS FROM ALERT_TASK WHERE ALERT_ID = ?";
	    pstm = connection.prepareStatement(sql);

	    // insert sql parameter
	    pstm.setString(1, AlertID);

	    // execute query
	    jrs = pstm.executeQuery();

	    while (jrs.next()) {
		status = jrs.getString("STATUS");
	    }
	} catch (Exception ex) {
	    String jsonOut = AlertID;
	    logger.error("FAILED. API : BCAAlert, method : POST, function: CheckAlertStatus, dataLog : " + jsonOut + ", Exception : " + ex.toString(), ex);
	} finally {
	    try {
		// close the opened connection
		if (pstm != null)
		    pstm.close();
		if (connection != null)
		    connection.close();
		if (jrs != null)
		    jrs.close();
	    } catch (Exception e) {

	    }
	}
	return status;
    }
}
