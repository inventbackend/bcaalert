package com.bca.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;

import adapter.LogAdapter;
import adapter.AlertAdapter;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String GetPing() {
	String ConnectionStatus = "true";
	return ConnectionStatus;
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public @ResponseBody model.mdlAPIResult CheckStatus(@RequestParam(value = "id", defaultValue = "") String AlertID,
	    @RequestParam(value = "wsid", defaultValue = "") String WSID, @RequestParam(value = "serial", defaultValue = "") String SerialNumber,
	    HttpServletResponse response) {
	model.mdlAPIResult mdlAlertResult = new model.mdlAPIResult();
	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
	model.mdlMessage mdlMessage = new model.mdlMessage();
	model.mdlStatus mdlStatus = new model.mdlStatus();
	Gson gson = new Gson();

	model.mdlLog mdlLog = new model.mdlLog();
	mdlLog.WSID = WSID;
	mdlLog.SerialNumber = SerialNumber;
	mdlLog.ApiFunction = "Alert";
	mdlLog.SystemFunction = "Alert";
	mdlLog.LogSource = "Webservice";
	mdlLog.LogStatus = "Failed";

	String APIName = "BCAAlert";
	String status = "";
	String jsonIn = "AlertID:" + AlertID + ", WSID:" + WSID + ", SerialNumber:" + SerialNumber;

	if (AlertID == null || AlertID.equals("")) {
	    mdlErrorSchema.ErrorCode = "01";
	    mdlMessage.Indonesian = "Alert ID kosong";
	    mdlMessage.English = mdlLog.ErrorMessage = "Alert ID empty";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlAlertResult.ErrorSchema = mdlErrorSchema;
	    mdlStatus.Status = status;
	    mdlAlertResult.OutputSchema = mdlStatus;
	    LogAdapter.InsertLog(mdlLog);
	    String jsonOut = gson.toJson(mdlAlertResult);
	    logger.error("FAILED. API : " + APIName + ", method : GET, parameterIn:" + jsonIn + ", jsonOut:" + jsonOut);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    return mdlAlertResult;
	}

	try {
	    status = AlertAdapter.CheckAlertStatus(AlertID);

	    if (status.equalsIgnoreCase("")) {
		mdlErrorSchema.ErrorCode = "02";
		mdlMessage.Indonesian = "Pengecekan alert status gagal";
		mdlMessage.English = mdlLog.ErrorMessage = "Check alert status failed";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlAlertResult.ErrorSchema = mdlErrorSchema;
		mdlStatus.Status = status;
		mdlAlertResult.OutputSchema = mdlStatus;
		String jsonOut = gson.toJson(mdlAlertResult);
		logger.error("FAILED. API : " + APIName + ", method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	    } else {
		mdlErrorSchema.ErrorCode = "00";
		mdlMessage.Indonesian = "Berhasil";
		mdlMessage.English = mdlLog.LogStatus = "Success";
		mdlErrorSchema.ErrorMessage = mdlMessage;
		mdlAlertResult.ErrorSchema = mdlErrorSchema;
		mdlStatus.Status = status;
		mdlAlertResult.OutputSchema = mdlStatus;

		mdlLog.LogStatus = "Success";
		String jsonOut = gson.toJson(mdlAlertResult);
		logger.info("SUCCESS. API : " + APIName + ", method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut);
	    }
	} catch (Exception ex) {
	    mdlErrorSchema.ErrorCode = "03";
	    mdlMessage.Indonesian = "Gagal memanggil service";
	    mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
	    mdlErrorSchema.ErrorMessage = mdlMessage;
	    mdlAlertResult.ErrorSchema = mdlErrorSchema;
	    mdlStatus.Status = status;
	    mdlAlertResult.OutputSchema = mdlStatus;
	    String jsonOut = gson.toJson(mdlAlertResult);
	    logger.error("FAILED. API : " + APIName + ", method : GET, jsonIn:" + jsonIn + ", jsonOut:" + jsonOut + ", Exception : " + ex.toString(), ex);
	    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}
	LogAdapter.InsertLog(mdlLog);
	return mdlAlertResult;
    }
}
